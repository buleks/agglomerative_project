import sys
sys.path.append("../")
import unittest

import numpy as np
import matplotlib.pyplot as plt
from agglomerative_clustering import Cluster
from agglomerative_clustering import Algorithm as Alg
from agglomerative_clustering import ClusterPair as CP


class TestAggloClast(unittest.TestCase):

  def test_create_clasters(self):
      clust = Cluster.Cluster([0,0])
      clusttmp = Cluster.Cluster([1,1])
      clust.addchild(clusttmp);
      clust.setparent(clusttmp);
      self.assertEqual(clust.getchildren()[0],clusttmp);
      self.assertEqual(clust.getparent(),clusttmp);
      self.assertFalse(clust.isleaf())


  def test_marge_clasters(self):
      clust1 = Cluster.Cluster([0,0])
      clust2 = Cluster.Cluster([3,4])
      dist = 5
      pair = CP.ClusterPair(clust1,clust2,dist);
      clustnew = pair.agglomerate();
      self.assertEqual(len(clustnew.getchildren()),2);
      self.assertEqual(clustnew.getchildren()[0],clust1);
      self.assertEqual(clustnew.getchildren()[1],clust2);


  def test_distance(self):
      alg = Alg.Algorithm()
      a = [0,0]
      b = [3,4]
      self.assertEqual(alg.euclidean_distance(a,b),5)

  def test_distance_with_bad_arg(self):
      alg = Alg.Algorithm()
      a = [0,0,0]
      b = [2,3]
      #self.assertEqual(alg.euclidean_distance(a,b),"Arguments different dimensions")# rzuca wyjatek dlatego test sie nie udaje

  def test_algorithm_2D(self):
      coordinates= np.transpose([[1,1,0,3,5,5,-4],[1,2,0,3,5,0,3]])
      correctResult = [4, 4, 4, 4, 1, 2, 3]
      alg = Alg.Algorithm(4,"single")
      X = np.transpose([[1,1,0,3,5,5,-4],[1,2,0,3,5,0,3]])
      alg.execute(X)
      result = alg.getlabels()
      self.assertEqual(result,correctResult)


  def test_algorithm_1D(self):
      correctResult = [1,1,1,1,1,2,1,1,2,1,1,1,2]
      coordinates = np.transpose(np.matrix([1,3,5,67,8,989,45,34,676,4,8,0,999]))
      alg = Alg.Algorithm(2,"single")
      alg.execute(coordinates)
      result = alg.getlabels()
      self.assertEqual(result,correctResult)


suite = unittest.TestLoader().loadTestsFromTestCase(TestAggloClast)
unittest.TextTestRunner(verbosity=2).run(suite)
