import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from agglomerative_clustering import Algorithm as a
from sklearn import cluster, datasets

alg = a.Algorithm(2,"single");

#punkty jednowymiarowe
#X=np.matrix([1, 2, 5, 9])

#punkty dwuwymiarowe
#pierwszy punkt to (1,1) drugi (1,2) itd..
X=[[1,1,0,3,5,5,-4,7,0,-5],[1,2,0,3,5,0,3,7,0,1]]

#punkty trojwymiarowe
#X=[[1,1,0,3,5,5,-4,7,0,-5],[1,2,0,3,5,0,3,7,0,1],[7,3,5,0,1,7,30,1,2,4]]

X=np.transpose(X)
alg.execute(X);
lab = alg.getlabels()

colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)


plt.scatter(X[:, 0], X[:, 1], color=colors[lab].tolist(), s=10)
plt.show()
