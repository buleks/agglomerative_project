import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from agglomerative_clustering import Algorithm as a


from sklearn import cluster, datasets
import matplotlib.pyplot as plt

circles = datasets.make_circles(n_samples=300, factor=.5,noise=.05)
X,Y = circles
alg = a.Algorithm(3);
alg.execute(X);
lab = alg.getlabels()

colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)

plt.scatter(X[:, 0], X[:, 1], color=colors[lab].tolist(), s=10)
plt.show()
