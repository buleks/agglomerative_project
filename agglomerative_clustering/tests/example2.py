import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from agglomerative_clustering import Algorithm as a
from sklearn import cluster, datasets

circles = datasets.make_circles(n_samples=300, factor=.5,noise=.05)
X,Y = circles
colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)

alg = a.Algorithm(3)
alg.execute(X)
lab = alg.getlabels()

average_linkage = cluster.AgglomerativeClustering(linkage="complete", affinity="euclidean", n_clusters=3)
average_linkage.fit(X)
y_pred = average_linkage.labels_.astype(np.int)

f, axarr = plt.subplots(1,2)
axarr[0].scatter(X[:, 0], X[:, 1], color=colors[lab].tolist(), s=10)
axarr[0].set_title("Nasz algorytm")
axarr[1].scatter(X[:, 0], X[:, 1], color=colors[y_pred].tolist(), s=10)
axarr[1].set_title("Algorytm z scikit-learn")

plt.show()
