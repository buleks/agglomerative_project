"""
This class represtents tha basic container used in algorithm.
It contains points, parent and children of cluster.
"""

class Cluster:
    cluster_counter = 1

    def __init__(self, points=None):
        self.points = points
        self.index = Cluster.cluster_counter
        Cluster.cluster_counter+=1
        self.children = list()
        self.parent = None

    def getchildren(self):
        return self.children

    def setparent(self, parent):
        self.parent = parent

    def getparent(self):
        return self.parent

    def addchild(self, cluster):
        self.children.append(cluster)

    def isleaf(self):
        return True if len(self.children) == 0 else False

    @staticmethod
    def resercounter():
        Cluster.cluster_counter=1


















