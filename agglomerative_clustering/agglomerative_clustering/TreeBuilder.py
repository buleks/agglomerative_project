from Cluster import Cluster
from DistanceMap import DistanceMap
from ClusterPair import ClusterPair

"""
One of the main class used by the algorithm. It generates tree of 
dependencies between cluster based on previously selected method 
of  linkage.
"""

class TreeBuilder:
    def __init__(self, clusters, distances,linkage):
        self.clusters = clusters
        self.distances = distances
        self.linkage = linkage
        
    """
    Checks if the tree is complete - tree is beeing build until "k" amount 
    of clusters are left
    :param k: final amout of clusters
    """
    def iscomplete(self, k):
        if len(self.clusters) == k:
            return True
        else:
            return False
    """
    Main function, which merges two clusters and removes them from list of
    clusters and then appends newly created pair.
    """
    def concatenate(self):
        mindistpair = self.distances.getfirst()
        if mindistpair is not None:
            self.clusters.remove(mindistpair.lCluster)
            self.clusters.remove(mindistpair.rCluster)
            leftclust = mindistpair.lCluster
            rightclust = mindistpair.rCluster
            newcluster = mindistpair.agglomerate()
            for clust in self.clusters:
                firstpair = self.findpair(clust, leftclust)
                secondpair = self.findpair(clust, rightclust)
                newdist = self.linkage.calculate(firstpair.getLinkageDistance(),secondpair.getLinkageDistance())
                self.distances.remove(firstpair)
                self.distances.remove(secondpair)
                newpair = ClusterPair(clust, newcluster, newdist)
                self.distances.add(newpair);
            self.clusters.append(newcluster)

    def getroot(self):
        if self.iscomplete():
            self.clusters.get(0)
        else:
            raise RuntimeError("Root not available")

    def findpair(self,first,second):
        if(first.index < second.index):
            i1 = first.index
            i2 = second.index
        else:
            i2 = first.index
            i1 = second.index
        hash = '{0}_{1}'.format(str(i1), str(i2))
        return self.distances.getpair(hash)
