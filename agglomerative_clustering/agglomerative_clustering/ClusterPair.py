
"""
Class used for merging two clusters together. Used only when tree is beeing build.
"""
from Cluster import Cluster

class ClusterPair:

    def __init__(self, left, right, distance):
        self.lCluster = left
        self.rCluster = right
        self.linkageDistance = distance
        self.hash = '{0}_{1}'.format(str(left.index), str(right.index))
        self.removed = False;

    def __cmp__(self, other):
        if self.getLinkageDistance() > other.getLinkageDistance():
            return 1
        elif self.getLinkageDistance() < other.getLinkageDistance():
            return -1
        else:
            return 0

    def getlCluster(self):
        return self.lCluster

    def setrCluster(self,rightCluster):
        self.rCluster = rightCluster
    
    def getLinkageDistance(self):
        return self.linkageDistance

    def agglomerate(self):
        cluster = Cluster()
        cluster.addchild(self.lCluster)
        cluster.addchild(self.rCluster)
        self.lCluster.setparent(cluster)
        self.rCluster.setparent(cluster)
        return cluster


