from DistanceMap import DistanceMap
import numpy as np
from Cluster import Cluster
import math
from ClusterPair import ClusterPair
from TreeBuilder import TreeBuilder
from abc import ABCMeta, abstractmethod

class SingleLinkage:
    def calculate(self, a, b):
        if a > b:
            return b
        else:
            return a

class CompleteLinkage:
    def calculate(self, a, b):
        if a < b:
            return b
        else:
            return a

class Algorithm:
    """
    This constructor configures algorithm
    :param k: final amout of clusters
    If this value is one, algorigthm merges all points to one group
    :param linkage This argument is used in order to choose linkage strategy. Possible values:complete,single
    """
    def __init__(self,k = 2,linkage="complete"):
        self.clusters = []
        self.distances = DistanceMap()
        if not isinstance(k, int):
            raise RuntimeError("K must be integer")
        if k == 0:
            raise RuntimeError("K must be greater than 0")
        self.k = k
        if linkage == "complete" :
            self.linkage = CompleteLinkage()
        elif linkage == "single" :
			self.linkage = SingleLinkage()
        else:
            raise RuntimeError("Incorrect linkage. Possible values: single,complete")
        Cluster.resercounter()

    def __create_clusters(self, X):
        dim = np.shape(X)
        self.points_count = dim[0]
        if self.k > self.points_count:
            raise RuntimeError("K is greater then number of points")
        dimension = dim[1]
        for i in range(self.points_count):
            cluster = Cluster(X[i, :])
            self.clusters.append(cluster);

    def __create_Linkages(self):
        clusters_count = len(self.clusters)
        for i1 in range(clusters_count):
            for i2 in range(i1 + 1, clusters_count):
                cluster1 = self.clusters[i1]
                cluster2 = self.clusters[i2]
                distance = self.euclidean_distance(cluster1.points, cluster2.points)
                pair = ClusterPair(cluster1, cluster2, distance)
                self.distances.add(pair)
              
    """
    This function executes the algorithm. The amount of time spend on exution of algorithm is dependent
    on the number of points given.
    :param X: Vector of points to analyze. In one dimensional space dataset of points must be 
    prepared as shown below:
    X=np.matrix([1, 2, 5, 9])
    X=np.transpose(X)
    In two dimensional space data looks like:
    X=[[1,1,0,3,5,5,-4,7,0,-5],[1,2,0,3,5,0,3,7,0,1]]
    X=np.transpose(X)
    This format of dataset is compatible with scikit-learn module.
    """
    def execute(self, X):
        self.__create_clusters(X)
        self.__create_Linkages()
        tree = TreeBuilder(self.clusters, self.distances,self.linkage)
        while not tree.iscomplete(self.k):
            tree.concatenate()
       
    """
    This method calculates euclidean distance between two point a and b.
    :returns: Distance between points
    """
    def euclidean_distance(self, a, b):
        if len(a) == len(b):
            dim = len(a)
            accumulator = 0
            for i in range(dim):
                diff = a[i] - b[i]
                accumulator += math.pow(diff, 2)
            return math.sqrt(accumulator)
        else:
            raise ValueError("Arguments different dimensions")

    """
    This method returns clustering result. Result format is the same as of method labels_.astype(np.int) from scikit-learn library.
    :returns: Array vector. Length of this array is the same as the number of points. Each element is an integer value which corresponds to the number of group.
    For example l=[1,2,2,1] means that l[0] - first point from input data belongs to group 1,second point - l[1] belongs to group 2 etc..
    """
    def getlabels(self):
        if len(self.clusters) == 0:
            raise ValueError("Before get labels, execute must be called")
        result = [0] * self.points_count
        group_index = 1
        for c in self.clusters:
            self.__update_labels(c,group_index,result)
            group_index += 1
        return result

    def __update_labels(self,clust,g,labels):
        if not clust.isleaf():
            left = clust.children[0]
            right = clust.children[1]
            self.__update_labels(left, g, labels)
            self.__update_labels(right, g, labels)
        else:
            labels[clust.index-1] = g
