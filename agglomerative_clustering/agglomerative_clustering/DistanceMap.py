import Queue as Q

"""
Class used for storing the distances calculated by linkage method in Algorithm.py    
"""
class DistanceMap:
    def __init__(self):
        self.dictionary = {}
        self.data = Q.PriorityQueue()
        
    def add(self, pair):
        self.dictionary[pair.hash] = pair
        self.data.put(pair)

    def show(self):
        while not self.data.empty():
            x = self.data.get()
            print x.hash + ":" + str(x.linkageDistance)

    def getfirst(self):
        first = self.data.get()
        while first is not None and first.removed is True:
            first = self.data.get()
        if not first:
            return None
        self.dictionary.pop(first.hash)
        return first

    def getpair(self, hash):
        return self.dictionary[hash]

    def remove(self, pair):
        pair.removed = True
        self.dictionary.pop(pair.hash)
